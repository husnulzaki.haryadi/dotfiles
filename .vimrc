" load plugins
call plug#begin('~/.vim/plugged')

Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'

" auto-complete
if has('nvim')
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
  Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif
Plug 'zchee/deoplete-jedi'

" General functionalities
Plug 'Raimondi/delimitMate'             " Provides insert mode auto-completion for quotes, parens, brackets, etc.
Plug 'Yggdroot/indentLine'              " Display indentation level with vertical line
Plug 'tpope/vim-surround'               " Quoting/parenthezing made easier
Plug 'tpope/vim-repeat'                 " Enable repeating on supported plugins (must have for vim-surround).
Plug 'bronson/vim-visual-star-search'   " Star search searches selection on visual mode.
Plug 'ntpeters/vim-better-whitespace'   " Highlight (and correct) trailing whitespaces
Plug 'wellle/targets.vim'               " Provides additional text objects
Plug 'elzr/vim-json'                    " JSON highlighting
Plug 'ervandew/supertab'                " Utilize <Tab> for auto-completion
"Plug 'tadaa/vimade'                     " Fades inactive windows
" TODO: Fix bug between multicursor and delimitMate (assign different keys maybe?)
" Plug 'terryma/vim-multiple-cursors'     " Utilize multiple cursors
" Plug 'nelstrom/vim-visual-star-search'  " More stable substitution of multicursor.

" General coding tools
Plug 'scrooloose/syntastic'             " Robust syntax checker

" Git integrations
Plug 'tpope/vim-fugitive'               " Definitive Git plugin for Vim
Plug 'airblade/vim-gitgutter'           " Display git diff and hunks

" Python specific plug-ins
Plug 'davidhalter/jedi-vim'             " For Python go-to function
Plug 'vim-python/python-syntax'         " Python syntax highlighting
" Plug 'nvie/vim-flake8'                  " Python syntax and style checker.
Plug 'Vimjas/vim-python-pep8-indent'    " Modifies Vim’s indentation behavior to comply with PEP8

" Better status bar
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Colours and themes
"Plug 'joshdick/onedark.vim'
"Plug 'altercation/vim-colors-solarized'
Plug 'tomasr/molokai'
Plug 'sickill/vim-monokai'

call plug#end()

"==================== Settings ====================
set nocompatible                " be iMproved, required
set updatetime=100              " Set updatetime to 100ms, handy for gitgutter and the like.

syntax enable
filetype plugin indent on
colorscheme molokai
" colorscheme default

set wildmode=longest:full,full

set encoding=utf-8

set ruler                       " Show the cursor position all the time
set cursorline                  " Highlight current line
set colorcolumn=88              " set ruler at 88th column (no relation with `set ruler`)

set number relativenumber
set showcmd                     " Show me what command I'm typing
set showmode                    " Show current mode

set et
set tabstop=4
set shiftwidth=4
set expandtab

" More natural split opening
set splitbelow
set splitright

" Make backspace keeps deleting previous words
set backspace=indent,eol,start


"***** REMAP *****
let mapleader=","

" Toggle line numbers
noremap <F3> :set invnumber invrelativenumber<CR>

" Easier split navigations
inoremap <C-J> <Esc><C-W><C-J>
inoremap <C-K> <Esc><C-W><C-K>
inoremap <C-L> <Esc><C-W><C-L>
inoremap <C-H> <Esc><C-W><C-H>

noremap <C-J> <Esc><C-W><C-J>
noremap <C-K> <Esc><C-W><C-K>
noremap <C-L> <Esc><C-W><C-L>
noremap <C-H> <Esc><C-W><C-H>

" Double <o> enter new line without entering Insert mode
nmap oo o<Esc>
nmap OO O<Esc>

" Double <Esc> clear selection
nnoremap <silent> <Esc><Esc> <Esc>:nohlsearch<CR><Esc>

" Delete one word with Ctrl + Backspace
noremap! <M-BS> <C-w>

" Ctrl+v for visual mode right from Insert mode
imap <C-v> <Esc>v

" Use leader to delete into blackhole register
nnoremap <leader>d "_d
xnoremap <leader>d "_d

" Utilize VSetSearch function
" vnoremap * :<C-u>call <SID>VSetSearch()<CR>/<CR>
" vnoremap # :<C-u>call <SID>VSetSearch()<CR>?<CR>

" Use Space to unfold
nnoremap <silent> <Space> @=(foldlevel('.')?'za':"\<Space>")<CR>
vnoremap <Space> zf

"***** GLOBAL CONFIGURATION *****
let g:python3_host_prog  = '/usr/bin/python3'


"***** USER-DEFINED COMMAND *****
" Change current working directory
command! Cd cd %:p:h            " For all windows
command! Cdl lcd %:p:h          " For local/current windows only

" Show diff between buffer and file in system
command! D w !diff % -


"***** FILETYPE SETTINGS *****
" yaml
au! BufNewFile,BufReadPost *.{yaml,yml} set filetype=yaml foldmethod=indent
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab


"***** FUNCTIONS *****
" VSetSearch: Visual mode search
" function! s:VSetSearch()
"   let temp = @@
"   norm! gvy
"   let @/ = '\V' . substitute(escape(@@, '\'), '\n', '\\n', 'g')
"   " Use this line instead of the above to match matches spanning across lines
"   "let @/ = '\V' . substitute(escape(@@, '\'), '\_s\+', '\\_s\\+', 'g')
"   call histadd('/', substitute(@/, '[?/]', '\="\\%d".char2nr(submatch(0))', 'g'))
"   let @@ = temp
" endfunction


"***** PLUG-IN CONFIGURATION *****
" ==================== delimitMate ====================
let g:delimitMate_expand_cr = 1
let g:delimitMate_expand_space = 1
let g:delimitMate_smart_quotes = 1
let g:delimitMate_expand_inside_quotes = 0
let g:delimitMate_smart_matchpairs = '^\%(\w\|\$\)'

"==================== NerdTree ====================
" For toggling
nmap <C-x> :NERDTreeToggle<CR>
noremap <Leader>n :NERDTreeToggle<cr>
noremap <Leader>f :NERDTreeFind<cr>

let NERDTreeShowHidden=1

let NERDTreeIgnore=['\.vim$', '\~$', '\.git$', '.DS_Store']

" Close nerdtree and vim on close file
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif

" ==================== vim-json ====================
let g:vim_json_syntax_conceal = 0

" ==================== Completion =========================
" use deoplete for Neovim.
if has('nvim')
  let g:deoplete#enable_at_startup = 1
  let g:deoplete#ignore_sources = {}
  let g:deoplete#ignore_sources._ = ['buffer', 'member', 'tag', 'file', 'neosnippet']
  let g:deoplete#sources#go#sort_class = ['func', 'type', 'var', 'const']
  let g:deoplete#sources#go#align_class = 1


  " Use partial fuzzy matches like YouCompleteMe
  call deoplete#custom#source('_', 'matchers', ['matcher_fuzzy'])
  call deoplete#custom#source('_', 'converters', ['converter_remove_paren'])
  call deoplete#custom#source('_', 'disabled_syntaxes', ['Comment', 'String'])
endif

" Change selection background color to contrast the monokai background
highlight Pmenu ctermbg=8 guibg=#606060

" ==================== vim-multiple-cursors ====================
"let g:multi_cursor_use_default_mapping=0
"let g:multi_cursor_next_key='<C-i>'
"let g:multi_cursor_prev_key='<C-y>'
"let g:multi_cursor_skip_key='<C-b>'
"let g:multi_cursor_quit_key='<Esc>'
"
"" Called once right before you start selecting multiple cursors
"function! Multiple_cursors_before()
"  if exists(':NeoCompleteLock')==2
"    exe 'NeoCompleteLock'
"  endif
"endfunction

" Called once only when the multiple selection is canceled (default <Esc>)
"function! Multiple_cursors_after()
"  if exists(':NeoCompleteUnlock')==2
"    exe 'NeoCompleteUnlock'
"  endif
"endfunction

" ========= vim-better-whitespace ==================
" auto strip whitespace except for file with extention blacklisted
let blacklist = []
autocmd BufWritePre * StripWhitespace

" ========= supertab ==================
" Make tab iterate from top to bottom
let g:SuperTabDefaultCompletionType = "<C-n>"

" ========= vim-gitgutter ==================
" Default `delete` sign color is not looking clear enough in monokai theme
highlight GitGutterDelete ctermfg=231 ctermbg=237 cterm=bold guifg=#f8f8f2 guibg=#3c3d37 gui=none

" ========= syntastic ==================
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 0
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 1
let g:syntastic_enable_highlighting = 1
let g:syntastic_check_on_wq = 0

" Python specific settings
let g:syntastic_python_checkers = ['flake8', 'pylint', 'pydocstyle']
let g:syntastic_python_flake8_args = "--max-line-length=88"
let g:syntastic_python_pydocstyle_args = "--add-ignore=D202"

" ========= jedi-vim ==================

" disable autocompletion, cause we use deoplete for completion
let g:jedi#completions_enabled = 0

" open the go-to function in split, not another buffer
let g:jedi#use_splits_not_buffers = "right"

" command mappings (:help jedi-vim for more info)
let g:jedi#completions_command = "<C-Space>"
let g:jedi#goto_command = "<leader>d"
let g:jedi#goto_assignments_command = "<leader>g"
let g:jedi#goto_stubs_command = "<leader>s"
let g:jedi#goto_definitions_command = ""
let g:jedi#documentation_command = "K"
let g:jedi#rename_command = "<leader>r"
let g:jedi#usages_command = "<leader>n"


"***** COLORSCHEME SETTINGS *****
if match(get(g:, "color_name"), "molokai")
    hi MatchParen      ctermfg=208 ctermbg=88 cterm=bold
endif
