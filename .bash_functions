# File: $HOME/.bashrc_custom
# THIS FILE IS A USER-CUSTOM BASHRC FILE TO KEEP CLEAN THE DEFAULT ~/.barshrc FILE.
# PUT THERE ANY CUSTOM CODE MANUALLY ADDED BY YOU

# Activate and deactivate proxy environment variables
function setproxy() {
  export http_proxy="http://husnulzaki.wh:49148099@167.205.22.103:8080"
  export https_proxy="http://husnulzaki.wh:49148099@167.205.22.103:8080"
  export ftp_proxy="http://husnulzaki.wh:49148099@167.205.22.103:8080"

  export HTTP_PROXY="http://husnulzaki.wh:49148099@167.205.22.103:8080"
  export HTTPS_PROXY="http://husnulzaki.wh:49148099@167.205.22.103:8080"
  export FTP_PROXY="http://husnulzaki.wh:49148099@167.205.22.103:8080"

  export no_proxy="167.205.*"
  export no_proxy="*.itb.ac.id"
  export no_proxy="localhost"
  export no_proxy="127.*"
  echo 'proxy udah di setting coy.'
}

function unsetproxy() {
  unset http_proxy
  unset https_proxy
  unset ftp_proxy
  unset no_proxy
  unset HTTP_PROXY
  unset HTTPS_PROXY
  unset FTP_PROXY
  echo 'proxy udah ga di pake lagi.'
}

# Show the size of files in a sorted and human readable way
function duhsr() {
  if [ $# -eq 0 ]
  then
    du --max-depth=1 -h * | sort -rh
  else
    du --max-depth=1 -h "$1" | sort -rh
  fi
}
